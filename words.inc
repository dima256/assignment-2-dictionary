%include "colon.inc"

section .data

colon "third_word", third_word
db "third word explanation", 0

colon "second_word", second_word
db "second word explanation", 0

colon "first_word", first_word
db "first word explanation", 0

colon "String longer than 255 characters! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sed velit consequat, luctus justo non, ultrices metus. Morbi risus nibh, pharetra sodales suscipit a, varius vitae libero. Nulla in congue tellus, mollis pulvinar turpis.", linked_list_with_long_key
db "Value of key with 255 symbols", 0
