%include "lib.inc"
global find_word

section .text

%define POINT_SIZE 8

; Принимает два аргумента:
; * rdi - указатель на нуль-терминированную строку.
; * rsi - указатель на начало словаря.
; Если подходящее вхождение найдено, возвращает адрес начала вхождения в словарь (не значения), иначе возвращает 0.

find_word:
	push rbx
	push rdi
	mov rbx, rsi
	.iter:
		mov rdi, [rsp]
		mov rsi, rbx
		add rsi, POINT_SIZE
		call string_equals
		cmp rax, 0
		jne .return
        cmp qword[rbx], 0
        mov rbx, qword[rbx]
        jne .iter
	.return:
		mov rax, rbx
		pop rdi
		pop rbx
		ret