%define last_id 0

%macro colon 2
	%ifid %2
		%ifstr %1
			%2:
				dq last_id
				db %1, 0
			%define last_id %2
		%else
			%error "1-ый аргумент не строка"
		%endif
	%else
		%error "2-ой аргумент не метка"
	%endif
%endmacro
