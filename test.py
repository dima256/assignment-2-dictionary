#!/usr/bin/python3

import subprocess

inp_str = ["", "third_word", "kek", "first_word", "____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"]
out_str = ["", "third word explanation", "",  "first word explanation", ""]
err_str = ["Key wasn't found!", "", "Key wasn't found!", "", "Error reading stdin!"]

for i in range(len(inp_str)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=inp_str[i].encode())
    str_out = stdout.decode().strip()
    str_err = stderr.decode().strip()
    if str_out == out_str[i] and str_err == err_str[i]:
        print("Test "+str(i+1)+" passed")
    else:
        print("Test "+str(i+1)+' failed. ./main print {strout: "'+str_out+'", strerr: "'+str_err+'"}, expected: {strout: "'+out_str[i]+'", strerr: "'+err_str[i]+'"}')
