%include "lib.inc"
%include "dict.inc"

%include "words.inc"


section .bss
buffer: resb 256

section .rodata
READ_ERROR_STRING: db "Error reading stdin!", 0xA, 0
FIND_ERROR_STRING: db "Key wasn't found!", 0xA, 0

section .text

global _start
_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	test rax, rax
	jz .error_reading
	
	mov rdi, buffer
	mov rsi, linked_list_with_long_key
	call find_word
	test rax, rax
	jz .error_searching
	
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	add rdi, 1
	call print_string
	call print_newline
	xor rdi, rdi
	jmp exit
	
	.error_reading:
		mov rdi, READ_ERROR_STRING
		jmp .error_exit
	.error_searching:
		mov rdi, FIND_ERROR_STRING
	.error_exit:
		call print_string_error
		mov rdi, 1
		jmp exit
		
